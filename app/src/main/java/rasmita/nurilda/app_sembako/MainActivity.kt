package rasmita.nurilda.app_sembako

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelperUp : MediaHelperUp
    lateinit var sbkAdapter : AdapterSembako
    lateinit var satAdapter : ArrayAdapter<String>
    lateinit var  MediaHelperCam : MediaHelperCam
    var Fileuri = Uri.parse("")
    var namafile = ""
    var daftarSembako = mutableListOf<HashMap<String, String>>()
    var daftarSatuan = mutableListOf<String>()
    val url = "http://192.168.43.159/sembako/show_sembako.php"
    var url1 = "http://192.168.43.159/sembako/show_satuan.php"
    var url2 = "http://192.168.43.159/sembako/query_inupdel.php"
    var imStr = ""
    var pilihSatuan = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sbkAdapter = AdapterSembako(daftarSembako,this)
        mediaHelperUp = MediaHelperUp(this)
        lsSbk.layoutManager = LinearLayoutManager(this)
        lsSbk.adapter = sbkAdapter
        satAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarSatuan)
        spinner.adapter = satAdapter
        spinner.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnIn.setOnClickListener(this)
        btnUp.setOnClickListener(this)
        btnDel.setOnClickListener(this)
        btnFind.setOnClickListener(this)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        MediaHelperCam = MediaHelperCam()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload ->{
                requestPermition()
            }
            R.id.btnIn ->{
                queryInUpDel("insert")
            }
            R.id.btnUp ->{
                queryInUpDel("update")
            }
            R.id.btnDel -> {
                queryInUpDel("delete")
            }
            R.id.btnFind ->{
                showDataSbk(edProduk.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihSatuan = daftarSatuan.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihSatuan = daftarSatuan.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == MediaHelperCam.getRcCam()){
                imStr = MediaHelperCam.getBitmapToString(imgUpload,Fileuri)
                namafile = MediaHelperCam.getMyfile()
            }
        }
    }
    fun queryInUpDel(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataSbk("")
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("kode",edKode.text.toString())
                        hm.put("nmproduk",edProduk.text.toString())
                        hm.put("ukuran",edUkuran.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("satuan",pilihSatuan)
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("kode",edKode.text.toString())
                        hm.put("nmproduk",edProduk.text.toString())
                        hm.put("ukuran",edUkuran.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("satuan",pilihSatuan)
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("kode",edKode.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getSatuan(namaSatuan : String){
        val request = StringRequest(Request.Method.POST,url1,
        Response.Listener { response ->
            daftarSatuan.clear()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length() - 1)) {
                val jsonObject = jsonArray.getJSONObject(x)
                daftarSatuan.add(jsonObject.getString("satuan"))
            }
            satAdapter.notifyDataSetChanged()
        },
            Response.ErrorListener { error ->  })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataSbk(namaSbk: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarSembako.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sbk = HashMap<String, String>()
                    sbk.put("kode", jsonObject.getString("kode"))
                    sbk.put("nmproduk", jsonObject.getString("nmproduk"))
                    sbk.put("ukuran", jsonObject.getString("ukuran"))
                    sbk.put("satuan", jsonObject.getString("satuan"))
                    sbk.put("harga", jsonObject.getString("harga"))
                    sbk.put("url", jsonObject.getString("url"))
                    daftarSembako.add(sbk)
                }
                sbkAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi Kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nmproduk", namaSbk)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun requestPermition() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        Fileuri = MediaHelperCam.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaHelperCam.getRcCam())
    }


    override fun onStart() {
        super.onStart()
        showDataSbk("")
        getSatuan("")
    }
}
