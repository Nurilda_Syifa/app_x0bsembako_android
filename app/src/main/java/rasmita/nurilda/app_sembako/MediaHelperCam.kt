package rasmita.nurilda.app_sembako

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.ImageView
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class MediaHelperCam {

    var namafile = ""
    var fileUri = Uri.parse("")
    val RC_cam = 100


    fun getMyfile(): String{
        return this.namafile
    }

    fun getRcCam() : Int{
        return this.RC_cam
    }

    fun getOutputMediahelper() : File? {
        val mediaStorageDir = File(Environment.getExternalStoragePublicDirectory
            (Environment.DIRECTORY_DCIM),"InneSimpan")
        if(!mediaStorageDir.exists())
            if (!mediaStorageDir.mkdirs()){
                Log.e("mkdir","Gagal Membuat direktori")
            }
        val mediafile = File(mediaStorageDir.path+File.separator+"${this.namafile}")
        return  mediafile
    }

    fun getOutputFileUri() : Uri {
        this.namafile =   "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Date())+".jpg"
        this.fileUri = Uri.fromFile(getOutputMediahelper())
        return this.fileUri
    }

    fun bitmaptoString(bpm : Bitmap) : String{
        val OutputStream = ByteArrayOutputStream()
        bpm.compress(Bitmap.CompressFormat.JPEG,60,OutputStream)
        val byteArray= OutputStream.toByteArray()
        return Base64.encodeToString(byteArray,Base64.DEFAULT)
    }


    fun getBitmapToString(imv : ImageView, uri : Uri) : String {
        var bpm = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888)
        bpm = BitmapFactory.decodeFile(this.fileUri.path)

        var d = 720
        if (bpm.height > bpm.width){
            bpm = Bitmap.createScaledBitmap(bpm,
                (bpm.width*d).div(bpm.height),d,true)
        }else{
            bpm = Bitmap.createScaledBitmap(bpm,
                d,(bpm.height*d).div(bpm.width),true)
        }

        imv.setImageBitmap(bpm)
        return bitmaptoString(bpm)
    }


}