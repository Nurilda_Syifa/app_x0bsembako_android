package rasmita.nurilda.app_sembako

import android.graphics.Color
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterSembako(val dataSem : List<HashMap<String, String>>,
                     val mainActivity: MainActivity) :
        RecyclerView.Adapter<AdapterSembako.HolderSembako>(){
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterSembako.HolderSembako {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_sbk,p0,false)
        return HolderSembako(v)
    }

    override fun getItemCount(): Int {
        return dataSem.size
    }

    override fun onBindViewHolder(p0: AdapterSembako.HolderSembako, p1: Int) {
        val data = dataSem.get(p1)
        p0.txKode.setText(data.get("kode"))
        p0.txProduk.setText(data.get("nmproduk"))
        p0.txUk.setText(data.get("ukuran"))
        p0.txsat.setText(data.get("satuan"))
        p0.txharga.setText(data.get("harga"))
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener({
            val pos = mainActivity.daftarSatuan.indexOf(data.get("satuan"))
            mainActivity.spinner.setSelection(pos)
            mainActivity.edKode.setText(data.get("kode"))
            mainActivity.edProduk.setText(data.get("nmproduk"))
            mainActivity.edUkuran.setText(data.get("ukuran"))
            mainActivity.edHarga.setText(data.get("harga"))
            Picasso.get().load(data.get("url")).into(mainActivity.imgUpload)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)

    }
    class HolderSembako(v : View) : RecyclerView.ViewHolder(v){
        val txKode = v.findViewById<TextView>(R.id.txKode)
        val txProduk = v.findViewById<TextView>(R.id.txProduk)
        val txUk = v.findViewById<TextView>(R.id.txUk)
        val txsat = v.findViewById<TextView>(R.id.txSat)
        val txharga = v.findViewById<TextView>(R.id.txHarga)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}